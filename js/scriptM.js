// script.js

// Función para mostrar una alerta cuando se hace clic en un enlace
function mostrarAlerta() {
    alert("¡Has hecho clic en un enlace!");
}

// Asociar la función a los enlaces de la barra de navegación
document.addEventListener("DOMContentLoaded", function () {
    const enlaces = document.querySelectorAll(".nav a");

    enlaces.forEach(function (enlace) {
        enlace.addEventListener("click", mostrarAlerta);
    });
});
